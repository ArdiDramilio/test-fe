import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  public employee: Employee;
  
  constructor(private employeeService: EmployeeService){}

  ngOnInit(): void {
    // throw new Error('Method not implemented.');
  }

  public onAddEmployee(addForm : NgForm): void{
    this.employee = addForm.value;    
      this.employeeService.addEmployees(this.employee).subscribe(
        (response: Employee) => {
          console.log(response);
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        },
      );
    console.log(this.employee);
    window.location.reload();
  }
}
