import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.css']
})
export class DetailEmployeeComponent implements OnInit {
  employee: Employee;
  id: number;
  constructor(private employeeService: EmployeeService, private route:ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.id = Number(params['id']);
      this.getEmployee();
    });
  }
  getEmployee() {
    this.employeeService.getEmployeeById(this.id).subscribe(
      (response: Employee) =>{
        this.employee = response;
        // console.log(this.employee);
      },
      (error : HttpErrorResponse) =>{
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
  }

}
