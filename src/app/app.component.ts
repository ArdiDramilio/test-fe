import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title(title: any) {
    throw new Error('Method not implemented.');
  }
  public employees: Employee[];
  public employee: Employee;
  constructor(private employeeService: EmployeeService){}

  ngOnInit(): void {
    this.getEmployees();
  }

  public getEmployees():void{
    this.employeeService.getEmployees().subscribe(
      (response: Employee[]) =>{
        this.employees = response;
      },
      (error : HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }
  public fireEmployee(id: number):void{
    this.employeeService.getEmployeeById(id).subscribe(
      (response: Employee) =>{
        this.employee = response;
        if (this.employee.status == true) {
          this.employee.status = false;
        } else {
          this.employee.status = true;
        }
        console.log(this.employee);
        this.employeeService.updateEmployees(this.employee).subscribe(
          (response: Employee) => {
            console.log(response);
            window.location.reload();
          },
          (error: HttpErrorResponse) => {
            alert(error.message);
          },
        );
      },
      (error : HttpErrorResponse) =>{
        alert(error.message);
      }
    ); 
  }

  public deleteEmployees(id: number):void{
    this.employeeService.deleteEmployees(id).subscribe();
    window.location.reload();
  }
  
}
