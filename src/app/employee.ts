export interface Employee {
    id:number;
    name:string;
    email:string;
    age:number;
    status:boolean;
    imageUrl:string;
}