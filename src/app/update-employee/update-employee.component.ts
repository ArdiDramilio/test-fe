import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  employee: Employee;
  newEmployee:Employee;
  id: number;

  constructor(private employeeService: EmployeeService, private route:ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.id = Number(params['id']);
      // console.log(this.id);
      this.getEmployee();
    });
  }
  
  ngOnInit(): void {
  }

  public getEmployee(){
    this.employeeService.getEmployeeById(this.id).subscribe(
      (response: Employee) =>{
        this.employee = response;
        // console.log(this.employee);
      },
      (error : HttpErrorResponse) =>{
        alert(error.message);
      }
    );
  }
  
  public onUpdateEmployee(updateForm : NgForm): void{
    this.newEmployee = this.employee;
    if(updateForm.controls.name.value){
      this.newEmployee.name = updateForm.controls.name.value;
    }
    if(updateForm.controls.age.value){
      this.newEmployee.age = updateForm.controls.age.value;
    }
    if(updateForm.controls.email.value){
      this.newEmployee.email = updateForm.controls.email.value;
    }
    if(updateForm.controls.imageUrl.value){
      this.newEmployee.imageUrl = updateForm.controls.imageUrl.value;
    }
    // console.log(this.newEmployee); 
    // console.log(updateForm.controls.name.value);  
      this.employeeService.updateEmployees(this.newEmployee).subscribe(
        (response: Employee) => {
          console.log(response);
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        },
      );
    window.location.reload();
  }
}
